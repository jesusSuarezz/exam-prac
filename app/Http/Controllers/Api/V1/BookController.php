<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\ActualizarPacienteRequest;
use App\Http\Requests\V1\BookRequest;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BookResource::collection(Book::with('author')->get());
    }

    private function upload($image)
    {
        $path_info = pathinfo($image->getClientOriginalName());
        $book_path = 'images/book';

        $rename = uniqid() . '.' . $path_info['extension'];
        $image->move(public_path() . "/$book_path", $rename);
        return "$book_path/$rename";
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $book = new Book();
        $book->title = $request->title;

        /* $url_image = $this->upload($request->file('image'));
        $book->image = $url_image; */

        $book->description = $request->description;
        $book->genre = $request->genre;
        $book->language = $request->language;
        $book->number_of_pages = $request->number_of_pages;
        $book->save();

        $book->author()->attach($request->authors);

        return (new BookResource($book))->additional([
            'message' => 'Libro creado correctamente',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new BookResource(Book::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, Book $book)
    {
        $book = Book::findOrFail($book->id);
        $book->title = $request->title;
        $book->description = $request->description;
        $book->genre = $request->genre;
        $book->language = $request->language;
        $book->number_of_pages = $request->number_of_pages;
        $book->save();

        $book->author()->sync($request->authors);

        return (new BookResource($book))->additional([
            'message' => 'Libro actualizado correctamente',
        ])->response()->setStatusCode(202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();

        return (new BookResource($book))
            ->additional([
                'message' => 'Libro eliminado correctamente',
            ]);
    }
}
