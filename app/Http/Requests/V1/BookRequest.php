<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:70',
            'image' => 'image|max:1024',
            'description' => 'required|max:2000',
            'genre' => 'required|max:2000',
            'language' => 'required|max:2000',
            'number_of_pages' => 'required|max:2000',
            'authors' => 'required|array|min:1',
        ];
    }
}
