<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                'title' => 'Hackers de JAVA y J2EE',
                'description' => 'Desarrolla aplicaciones Java seguras',
                'genre' => 'Programación',
                'language' => 'Español',
                'number_of_pages' => '200',
                'authors' => ["1", "2", "3"],
                'created_at' => now()
            ],
            [
                'title' => 'Superman',
                'description' => 'Superman es un superhéroe',
                'genre' => 'Ciencia Ficción',
                'language' => 'Español',
                'number_of_pages' => '120',
                'authors' => ["4"],
                'created_at' => now()
            ],
        );
    }
}
