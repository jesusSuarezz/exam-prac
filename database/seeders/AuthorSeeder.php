<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert(
            [
                'name' => 'R. Leyman',
                'country' => 'USA',
                'created_at' => now()
            ],
            [
                'name' => 'B. Buege',
                'country' => 'USA',
                'created_at' => now()
            ],
            [
                'name' => 'R. Leyman',
                'country' => 'USA',
                'created_at' => now()
            ],
            [
                'name' => 'Stan Lee',
                'country' => 'USA',
                'created_at' => now()
            ],
        );
    }
}
