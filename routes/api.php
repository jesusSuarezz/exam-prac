<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\AuthorController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
/* 
Route::get('/v1/author', [AuthorController::class, 'index']);
Route::POST('/v1/author', [AuthorController::class, 'store']); */


Route::apiResource('v1/author', App\Http\Controllers\Api\V1\AuthorController::class);
Route::apiResource('v1/books', App\Http\Controllers\Api\V1\BookController::class);
